#ifndef ITEM_H
#define ITEM_H

struct Item;
struct Menu;

struct Item
{
	int id;
	std::string (*handler)();	
	std::string desc;
	std::string keyword;
	Item * next;
	Menu * sub_menu;
	
	// Options
	bool output_result;

};

struct Menu
{
	Item * first;
	Item * last;
	int size;
};

#endif
