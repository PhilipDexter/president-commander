#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include "operations.h"
#include "list.h"

using namespace std;

// A global pointer to the last item in the tree.
Item * current;

/* Uses the input stream and returns the response given by the user.
 */
string get_question();
/* Simply traverses through the linked list menu.
 */
void menu(Menu * tree, int level);
/* Takes in the pieces to construct a menu item, including the delegate function to be called 
 *     when selected, then adds the menu to the linked list
 */
Item * add(Menu * tree, int id, string (*handler)(), string desc, string keyword, bool output_result = true);
/* Displays the item. Calls the handler function for the menu item.
 */
void displayItem(Item * item);
/* Displays the menu. Show the main menu with all the root menu items.
 */
void displayMenu(Menu * menu);
/* Performs a simple search through the linked list, for the key.
 *     Can find matches using both the id and keyword.
 */
Item * search_list(Menu * tree, string key);
/* Constructs the menu. Explicitely uses the add function to build the menu
 *    one by one. Returns a pointer the newly created linkd list.
*/
Menu * build_tree();
 /* Loops through the menu, calls displayMenu, search_list, get_question and displayItem.
 */
void menuLoop(Menu * menu);

int main()
{

	unsigned int seed = (unsigned int)time(NULL);
	srand(seed);
	current = NULL;
	Menu * tree = build_tree();	

	while (1)
	{
		menuLoop(tree);
	}

	return 0;
}

void menuLoop(Menu * menu)
{
	displayMenu(menu);
	string ques = get_question();
	Item * found = search_list(menu, ques);
	if (found != NULL)
	{
		displayItem(found);
	}
}

void displayMenu(Menu * tree)
{
	menu(tree, 0);
	cout << endl << "Input: ";
}

Menu * build_tree()
{
	Menu * tree = new Menu;
	tree->first = NULL;
	tree->last = NULL;
	tree->size = 0;
	int* index = &tree->size;

	add(tree, *index, &get_time, "Get Current Time", "time");
	add(tree, *index, &get_weather, "Get Current Weather", "weather");
	add(tree, *index, &get_random_number, "Get Random Number", "random");
	add(tree, *index, &run_browser, "Open Web Browser", "browser", false);
	add(tree, *index, &run_im, "Open Pidgin", "im", false);
	
	Item * test_menu = add(tree, 6, &sub_menu_test, "Open Test Sub Menu", "menu");
	Menu * sub_test = new Menu;
	sub_test->first = sub_test->last = NULL;
	sub_test->size = 0;
	test_menu->sub_menu = sub_test;
	add(sub_test, 1, &get_time, "Show me the time!", "time");


	return tree;
}

void displayItem(Item * item)
{
	if (item->output_result)
		cout << item->handler() << endl;
	else
		item->handler();

	if (item->sub_menu != NULL)
	{
		menuLoop(item->sub_menu);
	}
}

void menu(Menu * tree, int level)
{
	Item * node = tree->first;
	cout << endl;
	while (node != NULL)
	{
		cout << node->id << ": " << node->desc << endl;
		node = node->next;
	}
}

Item* add(Menu * tree, int id, string (*handler)(), string desc, string keyword, bool output_result)
{
	Item * item = new Item;
	item->handler = handler;
	item->desc = desc + " (" + keyword + ")";
	item->id = id;
	item->next = NULL;
	item->keyword = keyword;
	item->output_result = output_result;
	item->sub_menu = NULL;

	if (tree->size == 0)
	{
		tree->first = item;
		tree->last = item;
	}
	else
	{
		tree->last->next = item;
		tree->last = tree->last->next;
	}

	tree->size++;
	
	return item;
}

Item * search_list(Menu * tree, string key)
{
	Item * node = tree->first;
	int id;

	while (node != NULL)
	{
		stringstream ss(key);
		ss >> id;
		// Set our fail to a -1 so our menu supports 0 as an id.
		if (ss.fail())
			id = -1;
		if (key == node->keyword || id == node->id)
		{
			return node;
		}
		node = node->next;
	}
	return NULL;
}

string get_question()
{
	string ques = "";
	cin >> ques;

	return ques;
}
